"""The Turtle Racing Game"""

from random import randint
from turtle import Screen, Turtle

COLORS = ["red", "orange", "yellow", "green", "blue", "purple"]
turtles = []
ycoord = -125
winner = ""
screen = Screen()
screen.setup(width=500, height=400)

user_bet = screen.textinput(
    "Make your bet", "Which turtle will win the race? Enter a color:"
).lower()

for i, color in enumerate(COLORS):
    turtles.append(Turtle(shape="turtle"))
    turtles[i].color(color)
    turtles[i].penup()
    turtles[i].goto(x=-230, y=ycoord)
    ycoord += 50

while winner == "":
    for i, color in enumerate(COLORS):
        turtles[i].forward(randint(0, 10))
        if round(turtles[i].xcor()) >= 220:
            winner = color

if user_bet == winner:
    print(f"You've won! The {winner} turtle is the winner!")
else:
    print(f"You've lost! The {winner} turtle is the winner!")

screen.exitonclick()
