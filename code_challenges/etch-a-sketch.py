"""Etch-A-Sketch"""

from turtle import Screen, Turtle

timmy = Turtle()
screen = Screen()


def move_forwards():
    """Move the turtle forwards"""
    timmy.forward(10)


def move_backwards():
    """Move the turtle backwards"""
    timmy.backward(10)


def move_clockwise():
    """Rotate the turtle clockwise"""
    timmy.right(10)


def move_counterclockwise():
    """Rotate the turtle counter-clockwise"""
    timmy.left(10)


def clear_drawing():
    """Reset the screen"""
    screen.reset()


screen.listen()
screen.onkey(move_forwards, "w")
screen.onkey(move_backwards, "s")
screen.onkey(move_clockwise, "d")
screen.onkey(move_counterclockwise, "a")
screen.onkey(clear_drawing, "c")
screen.exitonclick()
